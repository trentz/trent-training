#!/bin/bash 
echo `date -u`
while [[ "$#" -gt 0 ]]
do
  case $1 in
    -s|--subscription)
      subscriptionId="$2"
      ;;
    -r|--resourcegroupname)
      resourceGroupName="$2"
      ;;
    -u|--user)
      user="$2"
      ;;
    -p|--password)
      password="$2"
      ;;
  esac
  shift
done

if [ -z "$subscriptionId" ]
then
  subscriptionId=aa97e1ef-a1f4-4009-8219-d3f3f43ba6c6
fi 

if [ -z "$resourceGroupName" ]
then
  resourceGroupName='Nexus-Training'
fi

app_id=e90ff3ef-40fe-4c28-921a-0654b0143ae1
tenant=ab5050ee-afc4-4ff8-829e-926701b64dcd
pass_key=d8455f9c-eb1b-42c0-bbb5-e48e5630fcfa

az login --service-principal -u $app_id -p $pass_key --tenant $tenant

az account set --subscription $subscriptionId


az network public-ip create --name $user'-publicip' --resource-group $resourceGroupName --allocation-method Dynamic
az network nic create --name $user'-interface' --resource-group $resourceGroupName --subnet vsubnet --vnet-name vnet --network-security-group nsg --public-ip-address $user'-publicip'

az vm create \
  --resource-group $resourceGroupName \
  --name $user'-Nexus-VM' \
  --image "UbuntuLTS" \
  --admin-username $user \
  --admin-password $password \
  --nics $user'-interface'

