//job for azure
import groovy.json.JsonSlurper


def appId="fec1dad6-9d11-4329-bb2a-114e2520d7b4"
def displayName="nexus-qa"
def name="http://nexus-qa"
def password="ceae1925-df10-4359-8f5a-801a1144f3c8"
def tenant="ab5050ee-afc4-4ff8-829e-926701b64dcd"
def subsciptionId = "aa97e1ef-a1f4-4009-8219-d3f3f43ba6c6"

try{
    def token_url = "https://login.microsoftonline.com/${tenant}/oauth2/token"
    def token_raw = ["curl", "-X", "POST", "-d", "grant_type=client_credentials&client_id=${appId}&client_secret=${password}&resource=https%3A%2F%2Fmanagement.azure.com%2F",
      "https://login.microsoftonline.com/${tenant}/oauth2/token"].execute().text
    
    def jsonSlurper = new JsonSlurper()
    def token_json = jsonSlurper.parseText(token_raw)
    
    def snapshots_url = "https://management.azure.com/subscriptions/${subsciptionId}/providers/Microsoft.Compute/snapshots?api-version=2019-07-01"
    def snapshot_raw = ["curl", "-X", "GET", "-H", "Authorization: Bearer ${token_json.access_token}", "-H", "Content-Type: application/json", "${snapshots_url}"].execute().text

    def snapshots_data = jsonSlurper.parseText(snapshot_raw)
    List<String> snapshots_numbers = new ArrayList<String>()
    for(item in snapshots_data.value){
    	print(item.name)
    	print('\n')
    	if(item.name.contains('snapshotLegacyOsDisk')){
        	//def version_number  = item.name.findAll( /-?\d+\.\d*|-?\d*\.\d+|-?\d+/ )*.toDouble()[0]
            def version_number = item.name.split('-')[1]
    		snapshots_numbers.add(version_number)
    	}
    }
    return snapshots_numbers

}
catch (Exception e){
    print(e)
    return e
}

//job for dockerhub
import groovy.json.JsonSlurper
try{
	def user="alexparadigm"
	def password="e7969549-25db-49d6-bf3d-9724aff51555"

	def token_url = "https://hub.docker.com/v2/users/login/"
	def token_raw = ["curl", "-X", "POST", "-d", "username=${user}&password=${password}", "${token_url}"].execute().text
	def jsonSlurper = new JsonSlurper()
	def token_json = jsonSlurper.parseText(token_raw)
	
	def tags_url = "https://hub.docker.com/v2/repositories/wtsparadigm/nexusapi/tags"
	def tags_raw = ["curl", "-X", "GET", "-H", "Authorization: JWT ${token_json.token}", "-H", "Content-Type: application/json", "${tags_url}"].execute().text
	def tags_data = jsonSlurper.parseText(tags_raw)
	List<String> tags_list = new ArrayList<String>()
	for(item in tags_data.results){
		tags_list.add(item.name)
	}
	return tags_list
}
catch (Exception e){
	print('error')
    print(e)
    return e
}
