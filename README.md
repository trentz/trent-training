# nexus-customer-example

Example project for customer deployment setup for Nexus modules.

## Steps to create a customer project
1. [Fork](https://confluence.atlassian.com/display/BITBUCKET/Forking+a+Repository) or copy the content naming the new repository nexus*-customer*
1. Customize the *readme.md* file explaining who the customer is and what is specific or useful for others
1. Change the *APP_TITLE* config in the front end config files *config.override.json*
1. Setup all connection, api and settings in backend config files *appsettings-compose.json*
1. To use DataDog
	1. Remove the *WRONG_* prefix in the *DD_API_KEY* environment variable from [prod/docker-compose.yml](prod/docker-compose.yml) and/or [other/shared/docker-compose.yml](other/shared/docker-compose.yml)
	1. Also put the right tags in the *DD_TAGS* of the files

## Steps to deploy
1. Clone project on ubuntu host (prefer using the https instead of ssh url)
1. Make sure you are connected to DockerHub `docker login`
1. Create network `docker network create --driver overlay proxy`
1. Move in the project folder `cd nexus-customer-example
1. If production
	1. Move in the environment folder `cd prod`
	1. Deploy the stack `docker stack deploy --compose-file docker-compose.yml --with-registry-auth nexus`
1. If test or dev
	1. Move in the other folder `cd other`
	1. Deploy the shared stack `docker stack deploy --compose-file shared/docker-compose.yml --with-registry-auth nexus-shared`
	1. Deploy the environment stack (*test* for example) `docker stack deploy --compose-file test/docker-compose.yml --with-registry-auth nexus-test`

## Default access
Using *localhost* as example.

* Traefik Dashboard: [http://localhost:8080/]
* Portainer: [http://localhost/portainer/]
* Front end: [http://localhost/]
* GraphiQL: [http://localhost/api/graphiql]
* GraphQL endpoint: [http://localhost/api/graphql]
