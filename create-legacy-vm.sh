#!/bin/bash 
echo `date -u`
while [[ "$#" -gt 0 ]]
do
  case $1 in
    -s|--subscription)
      subscriptionId="$2"
      ;;
    -r|--resourcegroupname)
      resourceGroupName="$2"
      ;;
    -v|--versionnumber)
      versionNumber="$2"
      ;;
  esac
  shift
done

if [ -z "$subscriptionId" ]
then
  subscriptionId=aa97e1ef-a1f4-4009-8219-d3f3f43ba6c6
fi 

if [ -z "$resourceGroupName" ]
then
  resourceGroupName='Nexus-QA'
fi

echo 'Installing version: $versionNumber'
echo $versionNumber

app_id=fec1dad6-9d11-4329-bb2a-114e2520d7b4
tenant=ab5050ee-afc4-4ff8-829e-926701b64dcd
pass_key=ceae1925-df10-4359-8f5a-801a1144f3c8

az login --service-principal -u $app_id -p $pass_key --tenant $tenant

az account set --subscription $subscriptionId
az disk list -g $resourceGroupName

az vm delete -n nexusLegacyVM -g $resourceGroupName -y

az disk delete -n legacyOsDisk -g $resourceGroupName -y
az disk delete -n legacyDataDisk -g $resourceGroupName -y
az vm wait -n nexusLegacyVM -g $resourceGroupName --deleted

echo 'create new env'
osSnapshotId=$(az snapshot show --name snapshotLegacyOsDisk-$versionNumber --resource-group $resourceGroupName --query [id] -o tsv)
dataSnapshotId=$(az snapshot show --name snapshotLegacyDataDisk-$versionNumber --resource-group $resourceGroupName --query [id] -o tsv)
az disk create -g $resourceGroupName -n legacyOsDisk --source $osSnapshotId
az disk create -g $resourceGroupName -n legacyDataDisk --source $dataSnapshotId

az vm create -g $resourceGroupName \
             -n nexusLegacyVM \
             --attach-os-disk legacyOsDisk \
             --os-type windows \
             --nsg-rule RDP \
             --nics Nexus-Legacy-Network-Adapter \
             --attach-data-disks legacyDataDisk

echo `date -u`
