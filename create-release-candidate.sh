#!/bin/bash 

#ssh keys needs to be setted on the server running this. Bitbucket + dockerhub 
while [[ "$#" -gt 0 ]]
do
  case $1 in
    -v|--rcVersion)
      versionNumber="$2"
      ;;
	-p|--push)
      push="$2"
      ;;      
  esac
  shift
done

nexus_fe='nexusfrontend'
nexus_api='nexusapi/NexusAPI'

function frontend(){
	echo 'frontend code pull'

	cd ~/$nexus_fe

	echo working dir $(pwd)
	git checkout master
	git reset --hard HEAD
	git pull
	git tag -a rc$versionNumber -m "New release candidate for rc$versionNumber"
	git push --tags
	echo 'frontend branch'
	git checkout -b 'release-candidate-'$versionNumber

	#git commit -m 'creation of release candidate '$versionNumber
	git push -u origin 'release-candidate-'$versionNumber
}

function frontend_build_image(){
	# link to doc
	# https://confluence.myparadigm.com/pages/viewpage.action?spaceKey=NEXUS&title=Front+End+Development+HowTos
	# Replace NpmToken.XXXXX with your npm auth token user name.family-name
	cd ~/$nexus_fe
	echo 'frontend image'
	cp ./docker_build/.npmrc .npmrc

	sudo docker build --build-arg NPM_TOKEN=NpmToken.96e96cd6-0e38-3ff2-b3ba-fc2e75deffae -t wtsparadigm/nexusfrontend:rc-$versionNumber .
	# Replace .npmrc file
	git checkout .npmrc
}

function frontend_push_image(){
	cd ~/$nexus_fe
	echo 'push frontend image'
	docker push wtsparadigm/nexusfrontend:rc-$versionNumber
}

function backend(){
	echo 'backend code pull'
	cd ~/$nexus_api
	echo working dir $(pwd)
	
	git checkout master
	git reset --hard HEAD
	git pull 
	git tag -a rc$versionNumber -m "New release candidate for rc$versionNumber"
	git push --tags
	echo 'backend create branch'
	git checkout -b 'release-candidate-'$versionNumber
	git push -u origin 'release-candidate-'$versionNumber
}

function backend_image(){
    echo 'backend image'
    cd ~/$nexus_api
    echo working dir $(pwd)
    cp /NexusAPI/Dockerfile .
    docker build -t wtsparadigm/nexusapi:rc-$versionNumber .
    docker push wtsparadigm/nexusapi:rc-$versionNumber
}

#frontend;

if [ "$push" == "yes" ]; then
# needs to be called with sudo
  frontend_build_image;
  frontend_push_image;
  backend_image;
else
# needs to be called without sudo
  frontend;
  backend;
fi
