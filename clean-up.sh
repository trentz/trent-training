#!/bin/bash 
echo `date -u`
while [[ "$#" -gt 0 ]]
do
  case $1 in
    -s|--subscription)
      subscriptionId="$2"
      ;;
    -r|--resourcegroupname)
      resourceGroupName="$2"
      ;;
  esac
  shift
done

if [ -z "$subscriptionId" ]
then
  subscriptionId=aa97e1ef-a1f4-4009-8219-d3f3f43ba6c6
fi 

if [ -z "$resourceGroupName" ]
then
  resourceGroupName='Nexus-QA'
fi

app_id=fec1dad6-9d11-4329-bb2a-114e2520d7b4
tenant=ab5050ee-afc4-4ff8-829e-926701b64dcd
pass_key=ceae1925-df10-4359-8f5a-801a1144f3c8

az login --service-principal -u $app_id -p $pass_key --tenant $tenant

az account set --subscription $subscriptionId
az disk list -g $resourceGroupName

az vm delete -n nexusLegacyVM -g $resourceGroupName -y
az disk delete -n legacyOsDisk -g $resourceGroupName -y
az disk delete -n legacyDataDisk -g $resourceGroupName -y

echo `date -u`
