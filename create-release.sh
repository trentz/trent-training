#!/bin/bash 

#ssh keys needs to be setted on the server running this. Bitbucket + dockerhub 
while [[ "$#" -gt 0 ]]
do
  case $1 in
    -v|--rcVersion)
      versionNumber="$2"
      ;;
	-p|--push)
      push="$2"     
      ;;
    -r|--release)
      release="$2"
      ;;      
  esac
  shift
done

nexus_fe='nexusfrontend'
nexus_api='nexusapi/NexusAPI'

#git
function frontend(){
	echo 'frontend code pull'

	cd ~/$nexus_fe

	echo working dir $(pwd)
	git checkout master
	git reset --hard HEAD
	git pull
	git tag -a rc$versionNumber -m "r$versionNumber"
	git push --tags
	echo 'frontend branch'
	git checkout -b 'r'$versionNumber

	#git commit -m 'creation of release candidate '$versionNumber
	git push -u origin 'r'$versionNumber
}

#fe image
function frontend_build_image(){
	# link to doc
	# https://confluence.myparadigm.com/pages/viewpage.action?spaceKey=NEXUS&title=Front+End+Development+HowTos
	# Replace NpmToken.XXXXX with your npm auth token user name.family-name
	cd ~/$nexus_fe
	echo 'frontend image'
	cp ./docker_build/.npmrc .npmrc

	sudo docker build --build-arg NPM_TOKEN=NpmToken.96e96cd6-0e38-3ff2-b3ba-fc2e75deffae -t wtsparadigm/nexusfrontend:rc-$versionNumber .
	# Replace .npmrc file
	git checkout .npmrc
}

#fe image
function frontend_push_image(){
	cd ~/$nexus_fe
	echo 'push frontend image'
	docker push wtsparadigm/nexusfrontend:r$versionNumber
}

function backend(){
	echo 'backend code pull'
	cd ~/$nexus_api
	echo working dir $(pwd)
	
	git checkout master
	git reset --hard HEAD
	git pull 
	git tag -a r$versionNumber -m "r$versionNumber"
	git push --tags
	echo 'backend create branch'
	git checkout -b 'r'$versionNumber
	git push -u origin 'r'$versionNumber
}

function backend_image(){
    echo 'backend image'
    cd ~/$nexus_api
    echo working dir $(pwd)
    cp /NexusAPI/Dockerfile .
    docker build -t wtsparadigm/nexusapi:r$versionNumber .
    docker push wtsparadigm/nexusapi:r$versionNumber
}

if [ "$push" == "yes" ]; then
# needs to be called with sudo
  frontend_build_image;
  frontend_push_image;
  backend_image;
else
# needs to be called without sudo
  frontend;
  backend;
fi

