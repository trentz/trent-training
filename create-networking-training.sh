#!/bin/bash 
echo `date -u`
while [[ "$#" -gt 0 ]]
do
  case $1 in
    -s|--subscription)
      subscriptionId="$2"
      ;;
    -r|--resourcegroupname)
      resourceGroupName="$2"
      ;;
  esac
  shift
done

if [ -z "$subscriptionId" ]
then
  subscriptionId=aa97e1ef-a1f4-4009-8219-d3f3f43ba6c6
fi 

if [ -z "$resourceGroupName" ]
then
  resourceGroupName='Nexus-Training'
fi

app_id=e90ff3ef-40fe-4c28-921a-0654b0143ae1
tenant=ab5050ee-afc4-4ff8-829e-926701b64dcd
pass_key=d8455f9c-eb1b-42c0-bbb5-e48e5630fcfa

az login --service-principal -u $app_id -p $pass_key --tenant $tenant

az account set --subscription $subscriptionId

az network vnet create -g $resourceGroupName -n vnet

az network vnet subnet create --address-prefixes 10.0.0.0/24 --name vsubnet --resource-group $resourceGroupName --vnet-name vnet

az network nsg create -g $resourceGroupName -n nsg

az network nsg rule create --name webcpAPI --nsg-name nsg --priority 100 --resource-group $resourceGroupName --destination-port-ranges 4430 4432
az network nsg rule create --name port80 --nsg-name nsg --priority 101 --resource-group $resourceGroupName --destination-port-ranges 80
az network nsg rule create --name port443 --nsg-name nsg --priority 102 --resource-group $resourceGroupName --destination-port-ranges 443
az network nsg rule create --name rdp --nsg-name nsg --priority 103 --resource-group $resourceGroupName --destination-port-ranges 3389
az network nsg rule create --name sql --nsg-name nsg --priority 104 --resource-group $resourceGroupName --destination-port-ranges 1433
az network nsg rule create --name ssh --nsg-name nsg --priority 105 --resource-group $resourceGroupName --destination-port-ranges 22

az network nic create --name legacyInterface --resource-group $resourceGroupName --subnet vsubnet --vnet-name vnet --network-security-group nsg
